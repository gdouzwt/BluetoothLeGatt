package com.example.android.bishengbei;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SeekBar;
import android.widget.TextView;

import com.zerokol.views.JoystickView;
import com.zerokol.views.JoystickView.OnJoystickMoveListener;


public class JoystickActivity extends Activity {

//    private TextView angleTextView;
//    private TextView powerTextView;
//    private TextView directionTextView;
    private JoystickView joystick;
    private SeekBar seekBar;
    private TextView seekBarValue;
    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";
    private String mDeviceName;
    private String mDeviceAddress;
    private TextView deviceAddress;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joystick);

//        angleTextView = (TextView)findViewById(R.id.angle_textview);
//        powerTextView = (TextView)findViewById(R.id.power_textview);
//        directionTextView = (TextView)findViewById(R.id.direction_textview);

        final Intent it = getIntent();
        mDeviceName = it.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = it.getStringExtra(EXTRAS_DEVICE_ADDRESS);
        joystick = (JoystickView)findViewById(R.id.BLEJoystick);
        seekBar = (SeekBar)findViewById(R.id.seekBar);
        seekBarValue = (TextView)findViewById(R.id.seekBarValue_TextView);
        deviceAddress = (TextView)findViewById(R.id.deviceAddress);
        deviceAddress.setText(mDeviceAddress);

        getActionBar().setTitle(mDeviceName);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        joystick.setOnJoystickMoveListener(new OnJoystickMoveListener() {
            @Override
            public void onValueChanged(int angle, int power, int direction) {
//                angleTextView.setText(" " + String.valueOf(angle) + "°");
//                powerTextView.setText(" " + String.valueOf(power) + "%");
//                /*switch (direction) {
//                    case JoystickView.FRONT:
//                        directionTextView.setText(R.string.front_lab);
//
//                        break;
//
//                    case JoystickView.FRONT_RIGHT:
//                        directionTextView.setText(R.string.front_right_lab);
//                        break;
//
//                    case JoystickView.RIGHT:
//                        directionTextView.setText(R.string.right_lab);
//                        break;
//
//                    case JoystickView.RIGHT_BOTTOM:
//                        directionTextView.setText(R.string.right_bottom_lab);
//                        break;
//
//                    case JoystickView.BOTTOM:
//                        directionTextView.setText(R.string.bottom_lab);
//                        break;
//
//                    case JoystickView.BOTTOM_LEFT:
//                        directionTextView.setText(R.string.bottom_left_lab);
//                        break;
//
//                    case JoystickView.LEFT:
//                        directionTextView.setText(R.string.left_lab);
//                        break;
//
//                    case JoystickView.LEFT_FRONT:
//                        directionTextView.setText(R.string.left_front_lab);
//                        break;
//
//                    default:
//                        directionTextView.setText(R.string.center_lab);
//                }*/
            }
        }, JoystickView.DEFAULT_LOOP_INTERVAL);



        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                if (b) {
//                    Toast.makeText(getApplicationContext(),"Now progress: " + i, Toast.LENGTH_SHORT).show();
                    seekBarValue.setText(" " + i);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_joystick, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
